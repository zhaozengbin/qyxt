package cn.xluobo.business.mk.discount.service;

import cn.xluobo.business.mk.discount.repo.model.MkDiscountDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 优惠配置明细 服务类
 * </p>
 *
 * @author zhangby
 * @since 2020-08-21
 */
public interface IMkDiscountDetailService extends IService<MkDiscountDetail> {

}

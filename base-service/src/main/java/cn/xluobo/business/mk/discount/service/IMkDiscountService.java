package cn.xluobo.business.mk.discount.service;

import cn.xluobo.business.mk.discount.repo.model.MkDiscount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 优惠配置 服务类
 * </p>
 *
 * @author zhangby
 * @since 2020-08-21
 */
public interface IMkDiscountService extends IService<MkDiscount> {

}

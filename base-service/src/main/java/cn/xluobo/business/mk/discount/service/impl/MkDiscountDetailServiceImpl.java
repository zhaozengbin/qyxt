package cn.xluobo.business.mk.discount.service.impl;

import cn.xluobo.business.mk.discount.repo.model.MkDiscountDetail;
import cn.xluobo.business.mk.discount.repo.mapper.MkDiscountDetailMapper;
import cn.xluobo.business.mk.discount.service.IMkDiscountDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠配置明细 服务实现类
 * </p>
 *
 * @author zhangby
 * @since 2020-08-21
 */
@Service
public class MkDiscountDetailServiceImpl extends ServiceImpl<MkDiscountDetailMapper, MkDiscountDetail> implements IMkDiscountDetailService {

}

package cn.xluobo.business.mk.discount.service.impl;

import cn.xluobo.business.mk.discount.repo.model.MkDiscount;
import cn.xluobo.business.mk.discount.repo.mapper.MkDiscountMapper;
import cn.xluobo.business.mk.discount.service.IMkDiscountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠配置 服务实现类
 * </p>
 *
 * @author zhangby
 * @since 2020-08-21
 */
@Service
public class MkDiscountServiceImpl extends ServiceImpl<MkDiscountMapper, MkDiscount> implements IMkDiscountService {

}

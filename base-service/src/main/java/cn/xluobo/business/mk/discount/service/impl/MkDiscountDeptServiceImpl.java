package cn.xluobo.business.mk.discount.service.impl;

import cn.xluobo.business.mk.discount.repo.model.MkDiscountDept;
import cn.xluobo.business.mk.discount.repo.mapper.MkDiscountDeptMapper;
import cn.xluobo.business.mk.discount.service.IMkDiscountDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 活动校区 服务实现类
 * </p>
 *
 * @author zhangby
 * @since 2020-08-21
 */
@Service
public class MkDiscountDeptServiceImpl extends ServiceImpl<MkDiscountDeptMapper, MkDiscountDept> implements IMkDiscountDeptService {

}

package cn.xluobo.business.stock.goods.repo.mapper;

import cn.xluobo.business.stock.goods.repo.model.StockInfoChange;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 库存变动 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2021-01-09 02:10:14
 */
public interface StockInfoChangeMapper extends com.baomidou.mybatisplus.core.mapper.BaseMapper<StockInfoChange> {

}

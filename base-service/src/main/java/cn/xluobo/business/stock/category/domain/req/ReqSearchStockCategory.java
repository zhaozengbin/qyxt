package cn.xluobo.business.stock.category.domain.req;

import cn.xluobo.business.stock.category.repo.model.StockCategory;

import java.io.Serializable;

/**
 * @author ：zhangbaoyu
 * @date ：Created in 2020-01-14 17:30
 */
public class ReqSearchStockCategory extends StockCategory implements Serializable {
}

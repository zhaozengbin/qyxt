package cn.xluobo.business.stock.info.service;

import cn.xluobo.business.stock.info.repo.model.StockInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 当前库存 服务类
 * </p>
 *
 * @author zhangby
 * @since 2021-01-09 02:05:31
 */
public interface IStockInfoService extends com.baomidou.mybatisplus.extension.service.IService<StockInfo> {

}

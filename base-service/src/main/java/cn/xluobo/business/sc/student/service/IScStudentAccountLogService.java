package cn.xluobo.business.sc.student.service;

import cn.xluobo.business.sc.student.repo.model.ScStudentAccountLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 账户变更日志 服务类
 * </p>
 *
 * @author zhangby
 * @since 2020-08-21
 */
public interface IScStudentAccountLogService extends IService<ScStudentAccountLog> {

}

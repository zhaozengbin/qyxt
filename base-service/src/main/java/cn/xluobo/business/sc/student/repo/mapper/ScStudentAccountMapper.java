package cn.xluobo.business.sc.student.repo.mapper;

import cn.xluobo.business.sc.student.repo.model.ScStudentAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 学生余额账户 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2020-08-21
 */
public interface ScStudentAccountMapper extends BaseMapper<ScStudentAccount> {

}

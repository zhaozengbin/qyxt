package cn.xluobo.business.sys.admin.repo.mapper;

import cn.xluobo.business.sys.admin.repo.model.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2020-01-12
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}

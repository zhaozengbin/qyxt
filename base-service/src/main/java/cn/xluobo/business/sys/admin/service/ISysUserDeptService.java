package cn.xluobo.business.sys.admin.service;

import cn.xluobo.business.sys.admin.repo.model.SysUserDept;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户拥有的校区权限 服务类
 * </p>
 *
 * @author zhangby
 * @since 2020-08-07
 */
public interface ISysUserDeptService extends IService<SysUserDept> {

}

package cn.xluobo.business.sys.admin.repo.mapper;

import cn.xluobo.business.sys.admin.repo.model.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2020-01-12
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}

package cn.xluobo.business.sys.admin.service.impl;

import cn.xluobo.business.sys.admin.repo.mapper.SysUserDeptMapper;
import cn.xluobo.business.sys.admin.repo.model.SysUserDept;
import cn.xluobo.business.sys.admin.service.ISysUserDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户拥有的校区权限 服务实现类
 * </p>
 *
 * @author zhangby
 * @since 2020-08-07
 */
@Service
public class SysUserDeptServiceImpl extends ServiceImpl<SysUserDeptMapper, SysUserDept> implements ISysUserDeptService {

}

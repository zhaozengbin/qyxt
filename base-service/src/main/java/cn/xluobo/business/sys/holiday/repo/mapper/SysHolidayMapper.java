package cn.xluobo.business.sys.holiday.repo.mapper;

import cn.xluobo.business.sys.holiday.repo.model.SysHoliday;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 节假日表 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2020-09-15
 */
public interface SysHolidayMapper extends BaseMapper<SysHoliday> {

}

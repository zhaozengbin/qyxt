package cn.xluobo.business.sys.admin.repo.mapper;

import cn.xluobo.business.sys.admin.repo.model.SysDictData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字典数据表 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2020-01-15
 */
public interface SysDictDataMapper extends BaseMapper<SysDictData> {

}

package cn.xluobo.business.sys.admin.repo.mapper;

import cn.xluobo.business.sys.admin.repo.model.SysFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 附件表 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2020-03-01 10:09:44
 */
public interface SysFileMapper extends BaseMapper<SysFile> {

}

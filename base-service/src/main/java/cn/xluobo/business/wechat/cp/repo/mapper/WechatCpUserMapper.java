package cn.xluobo.business.wechat.cp.repo.mapper;

import cn.xluobo.business.wechat.cp.repo.model.WechatCpUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 企业微信成员 Mapper 接口
 * </p>
 *
 * @author xluobo
 * @since 2024-01-26 08:08:32
 */
public interface WechatCpUserMapper extends com.baomidou.mybatisplus.core.mapper.BaseMapper<WechatCpUser> {

}

package cn.xluobo.business.wechat.cp.repo.mapper;

import cn.xluobo.business.wechat.cp.repo.model.WechatCpContact;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 企业微信客户信息 Mapper 接口
 * </p>
 *
 * @author xluobo
 * @since 2024-01-25 05:53:20
 */
public interface WechatCpContactMapper extends com.baomidou.mybatisplus.core.mapper.BaseMapper<WechatCpContact> {

}

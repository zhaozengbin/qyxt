package cn.xluobo.business.wechat.cp.service;

import cn.xluobo.business.wechat.cp.repo.model.WechatCpUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 企业微信成员 服务类
 * </p>
 *
 * @author xluobo
 * @since 2024-01-26 08:08:32
 */
public interface IWechatCpUserService extends com.baomidou.mybatisplus.extension.service.IService<WechatCpUser> {

}

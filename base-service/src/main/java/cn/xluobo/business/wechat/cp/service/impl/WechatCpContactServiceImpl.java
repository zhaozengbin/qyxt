package cn.xluobo.business.wechat.cp.service.impl;

import cn.xluobo.business.wechat.cp.repo.model.WechatCpContact;
import cn.xluobo.business.wechat.cp.repo.mapper.WechatCpContactMapper;
import cn.xluobo.business.wechat.cp.service.IWechatCpContactService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 企业微信客户信息 服务实现类
 * </p>
 *
 * @author xluobo
 * @since 2024-01-25 05:53:20
 */
@Service
public class WechatCpContactServiceImpl extends ServiceImpl<WechatCpContactMapper, WechatCpContact> implements IWechatCpContactService {

}

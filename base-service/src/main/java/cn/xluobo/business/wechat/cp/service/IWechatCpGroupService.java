package cn.xluobo.business.wechat.cp.service;

import cn.xluobo.business.wechat.cp.repo.model.WechatCpGroup;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 企业微信标签组 服务类
 * </p>
 *
 * @author xluobo
 * @since 2024-01-25 05:53:33
 */
public interface IWechatCpGroupService extends com.baomidou.mybatisplus.extension.service.IService<WechatCpGroup> {

}

package cn.xluobo.business.wechat.cp.service.impl;

import cn.xluobo.business.wechat.cp.repo.model.WechatCpGroupTag;
import cn.xluobo.business.wechat.cp.repo.mapper.WechatCpGroupTagMapper;
import cn.xluobo.business.wechat.cp.service.IWechatCpGroupTagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 企业微信标签 服务实现类
 * </p>
 *
 * @author xluobo
 * @since 2024-01-25 05:55:40
 */
@Service
public class WechatCpGroupTagServiceImpl extends ServiceImpl<WechatCpGroupTagMapper, WechatCpGroupTag> implements IWechatCpGroupTagService {

}

package cn.xluobo.business.wechat.user.repo.mapper;

import cn.xluobo.business.wechat.user.repo.model.WechatUserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 微信用户信息 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2020-10-15
 */
public interface WechatUserInfoMapper extends BaseMapper<WechatUserInfo> {

}

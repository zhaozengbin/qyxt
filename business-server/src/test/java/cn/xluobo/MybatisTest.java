package cn.xluobo;

import cn.xluobo.business.sys.admin.repo.mapper.SysTenantMapper;
import cn.xluobo.business.sys.admin.repo.mapper.SysUserMapper;
import cn.xluobo.business.sys.admin.repo.mapper.SysUserTenantMapper;
import cn.xluobo.business.sys.admin.repo.model.SysTenant;
import cn.xluobo.config.tenant.TenantContextHolder;
import cn.xluobo.business.sys.admin.repo.model.SysUser;
import cn.xluobo.business.sys.admin.service.ISysUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MybatisTest {

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private SysUserTenantMapper userTenantMapper;
    @Autowired
    private SysTenantMapper tenantMapper;

    @Test
    public void saveUser(){
        SysUser sysUser = new SysUser();
        sysUser.setUsername("admin").setPassword("$2a$10$sk8FArfGO7QOSC/GkVzeMuiCPGEkDqUSM0hmtwPyHwbcdJFCvN4B6").setName("name").setPhone("phone").setEmailAddress("email");

        sysUserService.save(sysUser);
    }

    @Test
    public void tenant1List(){
        TenantContextHolder.setTenant("1");
        List<SysUser> sysUserList = sysUserService.list();
        System.out.println(sysUserList.size());
    }

    @Test
    public void tenant2List(){
        TenantContextHolder.setTenant("2");
        List<SysUser> sysUserList = sysUserService.list();
        System.out.println(sysUserList.size());
    }

    @Test
    public void testTenant(){
        userTenantMapper.selectUserDefaultTenant("1");
    }
    @Test
    public void saveTenant(){
        SysTenant sysTenant = new SysTenant();
        sysTenant.setTenantName("红领巾");
        sysTenant.setBeginTime(new Date());
        sysTenant.setEndTime(new Date());
        tenantMapper.insert(sysTenant);
    }
}

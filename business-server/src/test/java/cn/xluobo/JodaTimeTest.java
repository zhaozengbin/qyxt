package cn.xluobo;

import cn.xluobo.core.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.junit.Test;

/**
 * @author ：zhangbaoyu
 * @date ：Created in 2020/9/5 10:07
 */
@Slf4j
public class JodaTimeTest {

    @Test
    public void diffDays() {
        DateTime beginDate = DateUtil.yyyMMddDayBegin("2020-09-01");
        DateTime endDate = DateUtil.yyyMMddDayEnd("2020-09-01");
        Period period = new Period(beginDate,endDate, PeriodType.days());
        int days = period.getDays();
        log.info("2020-09-01 2020-09-02,diffDays = {}",days);
    }

    @Test
    public void week() {
        // 周日
        DateTime beginDate = DateUtil.yyyMMddDayBegin("2020-09-13");

        // 周1
        beginDate = DateUtil.yyyMMddDayBegin("2020-09-14");
        System.out.println(beginDate.getDayOfWeek());

        // 周2
        beginDate = DateUtil.yyyMMddDayBegin("2020-09-15");
        System.out.println(beginDate.getDayOfWeek());
    }

    /**
     * 隔周重复
     */
    @Test
    public void weekCycle() {
        DateTime beginDate = DateUtil.yyyMMddDayBegin("2020-12-13");

        DateTime endDate = DateUtil.yyyMMddDayBegin("2021-02-13");



        while (beginDate.isBefore(endDate)) {
            int dayOfWeek = beginDate.getDayOfWeek();

            System.out.println(beginDate.toString("yyyy-MM-dd"));

            if(dayOfWeek == 7) {
                beginDate = beginDate.plusDays(8);
            } else {
                beginDate = beginDate.plusDays(1);
            }

        }

    }
}
